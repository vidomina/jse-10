package com.ushakova.tm.repository;

import com.ushakova.tm.api.IProjectRepository;
import com.ushakova.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projectList = new ArrayList<>();

    @Override
    public List<Project> findAll() {
        return projectList;
    }

    @Override
    public void add(final Project project) {
        projectList.add(project);
    }

    @Override
    public void remove(final Project project) {
        projectList.remove(project);
    }

    @Override
    public void clear() {
        projectList.clear();
    }

}
