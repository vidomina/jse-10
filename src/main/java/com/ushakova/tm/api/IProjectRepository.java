package com.ushakova.tm.api;

import com.ushakova.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    void add(Project project);

    void remove(Project project);

    void clear();

}
