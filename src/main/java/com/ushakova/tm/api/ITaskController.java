package com.ushakova.tm.api;

public interface ITaskController {

    void showList();

    void create();

    void clear();

}
